using Xunit;


namespace ValidadorCPFTest
{
    public class ValidadorCPFTeste
    {
        [Theory, InlineData("11144477735")]
        public void ValidarCompleto(string cadeia)
        {
            Assert.True(CriarValidador(cadeia).Validar());
        }

        [Theory, InlineData("111.444.777-35")]
        public void ValidarCompletoComSinais(string cadeia)
        {
            Assert.True(CriarValidador(cadeia).Validar());
        }

        [Theory, InlineData("11144477705")]
        public void ValidarCompletoFalha(string cadeia)
        {
            Assert.False(CriarValidador(cadeia).Validar());
        }

        [Theory, InlineData("1a144477705")]
        public void ValidarCPFFalha(string cadeia)
        {
            Assert.False(CriarValidador(cadeia).Validar());
        }

        private ValidadorCPF.ValidadorCPF CriarValidador(string cadeia)
        {
            return new ValidadorCPF.ValidadorCPF(cadeia);
        }
    }
}