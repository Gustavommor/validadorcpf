﻿using System.Text.RegularExpressions;

namespace ValidadorCPF
{
    public class ValidadorCPF
    {
        private static string regexCPF = "^\\d{3}\\.?\\d{3}\\.?\\d{3}\\-?\\d{2}$";

        private static string regexReplace = @"[\.]{1}|[-]";

        private static int MODULO = 11;

        private static int PRIMEIRO_DIGITO = 9;

        private static int SEGUNDO_DIGITO = 10;

        private string cadeia;
        public ValidadorCPF(string cadeia)
        {
            this.cadeia = cadeia;
        }

        public bool Validar()
        {
            if (!ValidaArgumentoValido())
            {
                return false;
            }

            int[] cadeiaArray = ParaInteiro();
            if (CalculaDigito(PRIMEIRO_DIGITO) != cadeiaArray[PRIMEIRO_DIGITO])
            {
                return false;
            }
            if (CalculaDigito(SEGUNDO_DIGITO) != cadeiaArray[SEGUNDO_DIGITO])
            {
                return false;
            }

            return true;
        }

        private int[] ParaInteiro()
        {
            string cadeiaReplace = Regex.Replace(cadeia, regexReplace, "");
            return cadeiaReplace.ToArray().Select(c => Int32.Parse(c.ToString())).ToArray();
        }

        private bool ValidaArgumentoValido()
        {
            return Regex.IsMatch(cadeia, regexCPF);
        }

        private int SomaValores(int[] cadeia, int posicaoDigito, int fator)
        {
            var Soma = 0;
            for (int posicao = 0; posicao < posicaoDigito; posicao++, fator --)
            {
                Soma += cadeia[posicao] * fator;
            }
            return Soma;
        }

        private int CalculaDigito(int posicaoDigito)
        {
            int resultado = SomaValores(ParaInteiro(), posicaoDigito, posicaoDigito + 1) % MODULO;
            return 2 > resultado ? 0 : MODULO - resultado; 
        }
    }
}